#!/bin/sh

# tools for installing and managing multiple Haxe compilers
# 
# recommended setup - add this line to ~/.profile
# alias hxenv='. ~/dev/scripts/hx-shell-utils/hxenv.sh'

#configure some globals
HX_REPO_PATH_DEFAULT=~/repo
HX_REPO_PATH=
SVN_DIR=

HAXE_REPO_URL="http://haxe.org/file/"
NEKO_REPO_URL="http://nekovm.org/_media/"

HAXE_VERSION=
NEKO_VERSION=

NEKOPATH=
HAXEPATH=

VERBOSE=0
PRESERVE_EXISTING_HAXELIB_PATH=0

# store arguments
ARGS=($@)
ARGSC=$#

#functions

function getInstalledVersions()
{
	local __prefix=$1
	local __versions=''
	#echo "$__prefix"

	for f in "$HX_REPO_PATH/*"
	do
		#echo "file $f"
  		__versions+=$(echo $f | egrep -o "$__prefix"'-([0-9\.]+)' | cut -d - -f 2)
	done
	echo "$__versions"
}

function getPlatform()
{	
	local __version=$1
	local __platform=''
	local __arch=''
	local __majorVersion=${__version:0:1}
	#echo "majorversion $__majorVersion" 
	case $(uname) in 
		"Darwin") __platform="osx";;
		"Linux") __platform="linux";;
	esac

	if [[ "$__platform" = "linux" ]]; then

		if [[ "$__majorVersion" = "3" ]]; then
			__arch=$(arch)
			if [[ "$__arch" = "x86_64" ]]; then
				__platform="linux64"
			else
				__platform="linux32"
			fi
		fi
	fi

	echo "$__platform"
}

function getPlatformExtension()
{
	local __ext=''

	case $(uname) in 
		"Darwin") __ext="tar.gz";;
		"Linux") __ext="tar.gz";;
	esac
	echo "$__ext"
}

function replaceInPath()
{
	local __path=$1
	local __pattern=$2
	local __replace=$3
	local __newPath

	if [[ "$__path" =~ "$__pattern" ]]; then
		#echo "replace"
		__newPath=`echo "$__path" | sed -e 's|'"$__pattern"'\([0-9.]*\)|'"$__replace"'|g'`
	else
		#echo "prefix"
		__newPath=`echo "$__replace":"$__path"`
	fi

	echo "$__newPath"
}

function __setVersion()
{
	local __prefix=$1
	local __version=$2 
	local __binDir=${HX_REPO_PATH}
	
	local __pattern="$__binDir/$__prefix-"
	local __path="$__binDir/$__prefix-$__version"

	export PATH=$(replaceInPath "$PATH" "$__pattern" "$__path")
}

function setHaxeVersion()
{
	local __version=$1

	case "$__version" in 
		# if version starts with 'r' then we are installing a specific Haxe revision
		r*) installHaxeRevision ${__version:1} ;;
		# if version is 'trunk' then we are installing latest revision from svn
		trunk) installHaxeTrunk ;;
		# otherwise we just was to treat this as a Haxe release available from http://haxe.org/file
		?*) installHaxeRelease "$__version" ;;
	esac
}

function setNekoVersion()
{
	installNekoRelease $1
}


function installHaxeRevision()
{
	local __revision=$1
	echo "revision support not implemented"
}

function installHaxeTrunk()
{
	echo "trunk support not implemented"
}

function installHaxeRelease()
{
	local __version=$1
	local __versions=$(getInstalledVersions haxe)
	local __hasVersion=0
	local __array=()

	#echo $__versions

	if $(echo "$__versions" | egrep -i ^"$__version"$ 1>/dev/null 2>&1); then
		setLocalHaxeVersion "$__version"
	else
		echo "Searching for Haxe $__version"
		installHaxeVersion "$__version"
		setLocalHaxeVersion "$__version"
	fi
}

function installNekoRelease()
{
	local __version="$1"
	local __versions=$(getInstalledVersions neko)

	if $(echo "$__versions" | egrep -i ^"$__version"$ 1>/dev/null 2>&1); then
		setLocalNekoVersion "$__version"
	else
		installNeko "$__version"
		setLocalNekoVersion "$__version"
	fi

}

function setLocalHaxeVersion()
{
	local __version=$1
	__setVersion "haxe" "$__version"
		
	local __hxPath="${HX_REPO_PATH}/haxe-$__version"
	
	if [ ${__version:0:1} = "3" ]; then
		export HAXE_STD_PATH="$__hxPath/std"
		export HAXE_LIBRARY_PATH=	
	else
		export HAXE_STD_PATH=
		export HAXE_LIBRARY_PATH="$__hxPath/std"
	fi

	configureHaxelib "$__hxPath"

	echo "haxe:$__version"
}

function setLocalNekoVersion()
{
	__setVersion "neko" $1
	local __ldLibraryPath=''
	local __newLdLibraryPath=''
	local __nekoPath="${HX_REPO_PATH}/neko-$1"

	if [ $(getPlatform) = "osx" ]; then
		__ldLibraryPath=$DYLD_LIBRARY_PATH
	else
		__ldLibraryPath=$LD_LIBRARY_PATH
	fi

	if [ "$__ldLibraryPath" = "" ]; then
		__newLdLibraryPath="$__nekoPath"
	else
		__newLdLibraryPath=$(replaceInPath "$__ldLibraryPath" "${HX_REPO_PATH}/neko-" "$__nekoPath")
	fi

	if [ $(getPlatform) = "osx" ]; then
		export DYLD_LIBRARY_PATH="$__newLdLibraryPath"
	else
		export LD_LIBRARY_PATH="$__newLdLibraryPath"
	fi
	
	echo "neko:$__version"
}

function getHaxeVersionURL()
{
	local __version="$1"
	local __haxeFile="${HAXE_REPO_URL}haxe-$__version-$(getPlatform "$__version").$(getPlatformExtension)"
	echo "$__haxeFile"
}

function getNekoVersionURL()
{
	local __version="$1"
	local __nekoFile="${NEKO_REPO_URL}neko-$__version-$(getPlatform).$(getPlatformExtension)"
	echo "$__nekoFile"
}

function getFileURLStatus()
{
	local __fileURL="$1"
	local __responseCode=`curl -s -o /dev/null -I -w "%{http_code}" "$__fileURL"`
	echo "$__responseCode"
}

function installHaxeVersion()
{
	local __version="$1"
	local __haxeFileURL=$(getHaxeVersionURL $__version)
	local __responseCode=$(getFileURLStatus "$__haxeFileURL")
	#echo "$__responseCode" 
	case "$__responseCode" in
		"404") 
		echo "$__haxeFileURL does not exist - Installation aborted";
		_kill
		;;
		"200") 
		echo "installing $__version"
		#TODO call installHaxe not tested yet
		installHaxe "$__version"
		;;
	esac
}

function installNekoVersion()
{
	local __version="$1"
	local __nekoFileURL=$(getNekoVersionURL $__version)
	local __responseCode=$(getFileURLStatus "$__nekoFileURL")
	#echo "$__responseCode" 
	case "$__responseCode" in
		"404") _kill "$__nekoFileURL does not exist - Installation aborted" ;;
		"200") installNeko "$__version";;
	esac
}

function installHaxe()
{
	checkInstallDir

	local __version="$1"
	
	local __haxeInstallPath="$HX_REPO_PATH/haxe-$__version"
	local __haxeFilePath="$__haxeInstallPath.$(getPlatformExtension)"
	local __haxeFileURL=$(getHaxeVersionURL $__version)
	
	echo "Installing Haxe version $1"
	echo "Downloading $__haxeFileURL ... to $__haxeFilePath"
	
	curl -o "$__haxeFilePath" "$__haxeFileURL"
	if [ ! -d "$__haxeInstallPath" ]; then
		mkdir "$__haxeInstallPath"
	fi
	tar xvf "$__haxeFilePath" -C "$__haxeInstallPath" --strip-components 1
	rm "$__haxeFilePath"
}

function installNeko()
{
	checkInstallDir

	local __version="$1"
	local __nekoInstallPath="$HX_REPO_PATH/neko-$__version"
	local __nekoFilePath="$__nekoInstallPath.$(getPlatformExtension)"
	local __nekoFileURL=$(getNekoVersionURL $__version)

	echo "Installing Neko version $1"
	echo "Downloading $__nekoFileURL ..."

	curl -o "$__nekoFilePath" "$__nekoFileURL"
	if [ ! -d "$__nekoInstallPath" ]; then
		mkdir "$__nekoInstallPath"
	fi
	tar xvf "$__nekoFilePath" -C "$__nekoInstallPath" --strip-components 1
	rm "$__nekoFilePath"
}


#-------------------------------------------------------------------------#
## Config Stuff
#-------------------------------------------------------------------------#

HX_REPO_PATH=
hx_trunk_dir=


hx_trunk_dir_default=~/repo/src/haxe/trunk

hxenv_config=~/.hxenv

function checkInstallDir()
{
	if [ -f "$HOME/.hxenv" ]; then
		readConfig
		#echo "HX_REPO_PATH $HX_REPO_PATH"
		if [[ -z "$HX_REPO_PATH" ]]; then
			setupInstallDir
		fi
	else
		setupInstallDir
	fi

	if [[ -z "$HX_REPO_PATH" ]]; then
		_kill "HX_REPO_PATH is not set. Aborting"
	fi
}

function readConfig()
{
	while read propline ; do 
		# ignore comment lines
		echo "$propline" | grep "^#" >/dev/null 2>&1 && continue
		# if not empty, set the property using declare
		#echo "$propline"
		#[ ! -z "$propline" ] && declare $propline
		
		# we use eval to make the read config values global
		[ ! -z "$propline" ] && eval $propline
	done < $HOME/.hxenv
}

function setupInstallDir()
{
	if [[ -z "$HX_REPO_PATH" ]]; then
		
		read -e -p "Haxe and Neko install dir (defaults to $HX_REPO_PATH_DEFAULT) " choice
		case "$choice" in 
	  		"" ) 
				HX_REPO_PATH="$HX_REPO_PATH_DEFAULT" 
				mkdir -p "$HX_REPO_PATH";
				checkInstallDir 
				;;
	  		* ) HX_REPO_PATH="$choice"; checkInstallDir ;;
		esac

	elif [[ -d "$HX_REPO_PATH" ]]; then
		#echo "Created $HX_REPO_PATH"; 
		echo "HX_REPO_PATH=$HX_REPO_PATH" >> "$hxenv_config"
	else
		echo "$HX_REPO_PATH does not exist..."
		read -e -p "Create $HX_REPO_PATH [Y/n] ? " choice
		case "$choice" in 
	  		y|Y ) mkdir -p "$HX_REPO_PATH"; checkInstallDir ;;
	  		n|N ) HX_REPO_PATH=; checkInstallDir ;;
	  		* ) echo "invalid option $choice"; checkInstallDir ;;
		esac
	fi	
}

function configureHaxelib
{
	if [[ PRESERVE_EXISTING_HAXELIB_PATH -eq 0 ]]; then

		local __path="$1"
		local __libPath="$__path/lib"
		if [[ ! -d "$__libPath" ]]; then 
			mkdir "$__libPath"
		fi

		local __currentHaxelibPath=$(haxelib config)
		if [[ "$__libPath" != "$__currentHaxelibPath" ]]; then
			haxelib setup "$__libPath"
			echo "WARNING: haxelib repo has been reset to default for $HAXE_VERSION"
		fi
	fi
}


#---------------------------------------------------------------------------#



function usage()
{
cat << EOF
usage: $0 options

This script sets the haxe and neko version to use in the current shell.
e.g. $ source hxenv.sh --haxe 2.10 --neko 1.8.2 

OPTIONS:
   --haxe      haxe version
   --neko      neko version
   -v          verbose / version print
   -p          preserve current haxelib repo path (default is to switch haxelib repo path to ~/repo/haxe-<version>/lib)
EOF
}

function printReport()
{
	echo
	echo "The following environment variables are configured for this shell"
	echo
	echo "PATH  $PATH"
	echo "DYLD_LIBRARY_PATH (mac)  $DYLD_LIBRARY_PATH"
	echo "LD_LIBRARY_PATH (linux)  $LD_LIBRARY_PATH"
	echo "HAXE_LIBRARY_PATH (haxe < 3)  $HAXE_LIBRARY_PATH"
	echo "HAXE_STD_PATH (haxe >= 3)  $HAXE_STD_PATH"
}

function _kill()
{
	echo "$@"
	kill -SIGINT $$
}

function preflight()
{
	if hash curl 2>/dev/null; then
		c=1
	else
		_kill "It seems you have no 'curl' installed. Please install 'curl' and try again"
	fi
}

function main()
{
	preflight

	# parse options
	for ((i=0; i < $ARGSC; i++)); do
		arg=${ARGS[i]}
		case "$arg" in
			--haxe) ((i++)); HAXE_VERSION=${ARGS[i]} ;;
			--neko) ((i++)); NEKO_VERSION=${ARGS[i]} ;;
			-v) VERBOSE=1;;
			-h) usage; _kill ;;
			-p) PRESERVE_EXISTING_HAXELIB_PATH=1;;
			-*) echo "unsupported option $arg" ;;
			?*) echo "$arg" ;;
		esac
	done

	# validate options
	if [[ -z $HAXE_VERSION ]] || [[ -z $NEKO_VERSION ]] ; then
		
		if [ $VERBOSE = "1" ]; then
			printReport
		else
			usage
		fi
		_kill
	fi

	checkInstallDir

	
	setHaxeVersion "$HAXE_VERSION"
	setNekoVersion "$NEKO_VERSION"

	if [ $VERBOSE = "1" ]; then
		printReport
	fi
}

main
